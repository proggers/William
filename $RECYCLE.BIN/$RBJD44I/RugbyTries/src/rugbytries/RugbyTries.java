/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rugbytries;

import javax.swing.JOptionPane;

/**
 *
 * @author William
 */
public class RugbyTries {
    
    public static void main(String[] args) {

        int score = Integer.parseInt(JOptionPane.showInputDialog("Enter a score:"));
        int length = digits(score/3);
        
        int[] numbers;
        numbers = new int[(int)Math.pow(10,length)];
        int indiceNo = 0;
        int a,b,c;

        for(int set = 1; set<(int)Math.pow(1000, length); set++){
            a = set/(int)Math.pow(100,length);
            b =(set-a*(int)Math.pow(100,length))/(int)Math.pow(10,length);
            c = set-a*(int)Math.pow(100,length)-b*(int)Math.pow(10,length);
            
            if(3*a+5*b+7*c == score ){
                numbers[indiceNo] = set;
                indiceNo++;
            }

        }

        for(int count=0;count<indiceNo;count++) {

            int x = numbers[count]/(int)Math.pow(100,length);
            int y = (numbers[count]-x*(int)Math.pow(100,length))/(int)Math.pow(10,length);
            int z = numbers[count]-x*(int)Math.pow(100,length)-y*(int)Math.pow(10,length);

            System.out.println(x +" drop kicks, "+y+" tries and " +z+" converted tries.");
        
        }
        
        if (indiceNo == 0) {System.out.println("Not a valid score!");}

    }

    public static int digits(int num) {
        int digits = 1;
        while(true) {
            if (num%10==0) { num = num/10; digits++; } else { return digits;}
        }
    }

}
