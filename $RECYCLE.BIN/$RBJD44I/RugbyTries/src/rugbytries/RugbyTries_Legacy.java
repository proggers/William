/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rugbytries;

import javax.swing.JOptionPane;

/**
 *
 * @author William
 */
public class RugbyTries_Legacy {

    public static void main(String[] args) {

        int score = Integer.parseInt(JOptionPane.showInputDialog("Enter a score:"));
        int length = digits(score/3+1);
        
        int[] numbers;
        numbers = new int[100];
        int indiceNo = 0;
        int a,b,c;

        for(int set = 1; set<1000; set++){
            a = set/100;
            b =(set-a*100)/10;
            c = set-a*100-b*10;
            
            if(3*a+5*b+7*c == score ){
                numbers[indiceNo] = set;
                indiceNo++;
            }

        }

        for(int count=0;count<indiceNo;count++) {
            list(numbers[count]);
        }

    }

    public static int digits(int num) {
        int digits = 0;
        while(true) {
            if (num%10==0) { num = num/10; digits++; } else { return digits;}
        }
    }


    public static void list(int a){

        int x =a/100;
        int y =(a-x*100)/10;
        int z =a-x*100-y*10;

        System.out.println(x +" drop kicks, "+y+" tries and " +z+" converted tries.");

    }

}
