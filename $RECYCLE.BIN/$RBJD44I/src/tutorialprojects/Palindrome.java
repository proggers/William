/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tutorialprojects;

import javax.swing.JOptionPane;

/**
 *
 * @author User
 */
public class Palindrome {
    public static void main(String[] args) {
        int num = Integer.parseInt(JOptionPane.showInputDialog("Enter a number:"));
        int digits = digits(num);
        int[] numbers;
        numbers = new int[digits];
        int beginNum = 0;
        int endNum = digits - 1;
        boolean palindrome = true;
        int num2 = num;
        
        int digits2 = digits - 1;
        int indice = 0;
        
        
        while(true) {
           numbers[indice] = (int) (num2/Math.pow(10,digits2));
           num2 = (int) (num2 - numbers[indice]*Math.pow(10,digits2));
           indice++;
           digits2 = digits2 - 1;
           if (indice > endNum) {
               break;
           }
        }
        
        while (true) {
            
            if (beginNum > endNum) {
                break;
            }
            if (numbers[beginNum] == numbers[endNum]) {
                beginNum++;
                endNum = endNum - 1;
            } else {
                palindrome = false;
                break;
            }
            
            
            
        }
        
        System.out.println(palindrome);
       
    }
    public static int digits(int num) {
        
        boolean notchecked = true;
        int power = 0;
        
        while (notchecked) {
            if (num < Math.pow(10,power) && num >= Math.pow(10, power - 1)) {
                notchecked = false;
            } else {
                power = 1 + power;
            }
            
        }
        return power;
    }
}
