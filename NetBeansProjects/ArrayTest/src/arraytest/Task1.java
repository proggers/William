/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arraytest;

import javax.swing.JOptionPane;

/**
 *
 * @author 23723
 */
public class Task1 {
    public static void main(String[] args) {
        int[] runs = new int[10];
        for(int x = 0;x<10;x++){
            runs[x]=Integer.parseInt(JOptionPane.showInputDialog("Runs in inning: "+x+1));
        }
        int sum = 0;
        for(int x = 0;x<10;x++){
            sum += runs[x];
        }
        double ave = sum/10;
        System.out.println("Average: "+ave);
        double sumSquared = 0;
        for(int x = 0;x<10;x++){
            sum += runs[x]*runs[x];
        }
        double var = (sumSquared/10)-ave*ave;
        System.out.println("Variance: " + var);
        int higher = 0;
        for(int x = 0;x<10;x++){
            if (runs[x]>ave){
                higher++;
            }
        }
        System.out.println("No of innings higher than average: "+higher);
        int highest = 0;
        int highestInnings = 0;
        for(int x = 0;x<10;x++){
            if (runs[x]>highest){
                highestInnings =x+1;
                highest = runs[x];
            }
        }
        System.out.println("Highest scoring innings: "+highestInnings+"\t"+"Score: "+highest);
        int[] range = new int[11];
        for(int x = 0;x<10;x++){
            for(int y = 0;y<10;y++){
                if(runs[x]/10==y){
                    range[y]++;
                }
            }
        }
        for(int x = 0;x<10;x++){
            if(runs[x]>=100){
                range[10]++;
            }
        }
        for(int y = 0;y<10;y++){
            System.out.println("Interval: "+y*10+"-"+y+9+": "+range[y]);
        }
        System.out.println("Centuries: "+range[10]);
    }
}
