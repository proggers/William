/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arraytest21september;

/**
 *
 * @author 23723 - William Moolman - UIV
 * @date 21 September 2017
 * 
 */
public class ArrayTest21September {

    //Does various tests around probability theory
    public static void main(String[] args) {                            
        int[] arr = new int[60];                                                //initialises array of 60 ints
        
        for(int x = 0; x < arr.length;x++){                                     //Fills array
            arr[x]=(int) (Math.random()*2);                                     //Set array value to either 0 or 1
        }
        
        int heads = 0, tails = 0;                                               //0 is heads and 1 is tails, counters
        
        for(int x = 0; x < arr.length;x++){           
            switch(arr[x]){                                                     //counts heads and tails
                case 0: heads++;break;
                case 1: tails++;break;
            }
        }
        
        System.out.println(heads+" Heads, "+tails+" Tails");                    //prints results
        System.out.println("Heads "+heads*100/60+"% Tails "+tails*100/60+"%");  //prints results as percentages of 60
        
        int length = 1;                                                         //length of current consecutive ints
        int max = 0;                                                            //length of longest consecutive run in array
        boolean head = false;                                                   //is longest array
        int p = arr[0];
        
        for(int x = 1;x<arr.length;x++){
            if(p==arr[x]){
                length++;
            } else {
                if (length > max){
                    max = length;
                    if(arr[x-1]==0){
                        head = true;
                    } else {
                        head = false;
                    }
                }
                length = 1;
            }
            p = arr[x];
        }
        
        String k;
        if(head){
            k = "heads";
        } else {
            k = "tails";
        }
        
        System.out.println("There were "+max+" consecutive "+k+" generated.");
        
    }
   
}
