/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrays;

/**
 *
 * @author 23723
 */
public class Arrays {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        double[] myList = {1.5,0.7,2.2,3.1,1.6,0.9};
        System.out.println("Size of myList = "+ myList.length);
        System.out.println("First item in myList = " + myList[0]);
        System.out.println("Second item in myList = " + myList[1]);
        System.out.println("Third item in myList = " + myList[2]);
        System.out.println("Last item in myList = " + myList[myList.length-1]);
        
        double total = 0.0;
        for(int item = 0;item < myList.length;item++){
            total += myList[item];
        }
        System.out.println(total);
    }

    static String toString(int[] p) {
        String roturn = "["+p[0];
        for(int x = 1;x<p.length;x++){
            roturn += " ," + p[x];
        }
        roturn += "]";
        return roturn;
    }
    
}
