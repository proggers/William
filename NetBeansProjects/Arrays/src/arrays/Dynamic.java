/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrays;

import java.util.Collections;

/**
 *
 * @author 23723
 */
public class Dynamic {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int[] x = new int[100000];
        double total = 0;
        for(int i= 0;i < x.length;i++){
            x[i] = 1+(int)(1000000*Math.random());
        }
        int noTwos = 0;
        for(int i = 0; i < x.length;i++){
            if (x[i]==2){
                noTwos++;
            }
        }
        System.out.println("Number of 2's = " + noTwos);
        int max = x[0];
        for(int i = 0; i < x.length;i++){
            if (x[i]>max){
                max=x[i];
            }
        }
        
        System.out.println("Max number = "+max);
        int min = x[0];
        for(int i = 0; i < x.length;i++){
            if (x[i]<min){
                min=x[i];
            }
        }
        System.out.println("Min number = " +min);
    }
    
}
