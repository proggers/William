/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrays;

/**
 *
 * @author 23723
 */
public class MagicSquare {
    public static void main(String[] args) {
        run(4);
    }
    public static int[] run(int size) {
        int l = size;
        int[][]sq=new int[l][l];
        int[] rd = new int [l*l];
        int runs = 0;
        
        while(true){
            
            randm(rd);
            runs++;
            
            int count = 0;
            for(int x = 0; x < l;x++){
                for(int y = 0; y < l;y++){
                    sq[x][y]=rd[count];
                    count++;
                }
            }
            
            
            if(test(sq)){
                break;
            }
            System.out.println(runs);
        }
        for(int p = 0;p<l;p++){
            System.out.println(Arrays.toString(sq[p]));
        }
        System.out.println("");
        System.out.println("Runs: "+runs);
        return rd;
    }
    public static boolean test(int[][]x) {
        boolean yes = true;
        int l = x.length;
        int s = l*(l*l+1)/2;
        for(int o = 0;o<l;o++){
            if(sum(x,o,true)!=s){yes=false;}
        }
        for(int o = 0;o<l;o++){
            if(sum(x,o,false)!=s){yes=false;}
        }
        if(diagonal(x,true)!=s){yes = false;}
        if(diagonal(x,false)!=s){yes = false;}
        return yes;
    }  
    public static int sum(int[][] x, int y,boolean row) {
        int tot = 0;
        if(row){
            for(int p = 0;p<x.length;p++){
                tot += x[y][p];
            }
        } else {
            for(int p = 0;p<x.length;p++){
                tot += x[p][y];
            }
        }
        return tot;
    }
    public static int diagonal(int[][]x,boolean left) {
        int tot = 0;
        if(left){
            for(int u = 0;u<x.length;u++){
                tot += x[u][u];
            }
        } else {
            for(int u = 0;u<x.length;u++){
                tot += x[u][x.length-u-1];
            }
        }
        return tot;
    }
    public static void randm(int[] rd) {
        for(int i = 0; i<rd.length; i++){
                while(true){
                        
                    boolean allowed = true;
                    rd[i] = 1+(int)(Math.random()*rd.length);
                        
                    for(int o = 0;o<i;o++){
                        if(rd[i]==rd[o]){
                            allowed = false;
                        }
                    }
                        
                    if(allowed){
                        break;
                    }
                        
                }
            }
    }
}
