/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrays;

import javax.swing.JOptionPane;
import java.util.Arrays;

/**
 *
 * @author 23723
 */
public class OddEven {
    public static void main(String[] args) {
        int[] arr = new int[10];
        for(int q = 0; q<10;q++){
            int e = q+1;
            arr[q]=Integer.parseInt(JOptionPane.showInputDialog("Enter number "+e));
        }
        System.out.println(Arrays.toString(arr));
        int n = arr.length;  
        int temp = 0;
        for(int i=0; i < n; i++){ 
            for (int j = 1; j < (n - i); j++) {
                if (arr[j - 1]%2==0&& arr[j]%2!=0) {
                //swap elements  
                    temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                    
                }

            } 
        } 
        System.out.println(Arrays.toString(arr));
    }
}
