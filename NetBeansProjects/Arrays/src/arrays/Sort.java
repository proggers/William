/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrays;
import java.util.Arrays;
/**
 *
 * @author 23723
 */
public class Sort {
    
    public static void main(String[] args) {
        int[] arr = {5,-9,1,56,8,5,0,7,2340,-546,33,75};
        System.out.println("bubble: "+bubble(arr));
        //arr.length(arr.length-1)/2
        System.out.println("selection: "+selection(arr));
        //arr.length(arr.length-1)/2+arr.length
        System.out.println(Arrays.toString(arr));
    }
    
    public static void grid(int[] arr) {
        int[] arr2 = new int[arr.length];
        int[] arr3 = new int[arr.length];
        //ayy lmao Arrays.sort(arr);
        for(int x = 0;x<arr.length;x++){
            for(int y = 0;y<arr.length;y++){
                if(arr[x]>arr[y]){
                    arr2[x]++;
                }
                if(arr[x]==arr[y]&&x>y){
                    arr2[x]++;
                }
            }
        }
        
        for(int x = 0;x<arr.length;x++){
            arr3[arr2[x]]=arr[x];
        }
        for(int x = 0;x<arr.length;x++){
            arr[x]=arr3[x];
        }
    }
    
    public static int bubble(int[] arr) {
        int n = arr.length;  
        int temp = 0;
        int steps = 0;
        for(int i=0; i < n; i++){ 
            
            for (int j = 1; j < (n - i); j++) {
                steps++;
                if (arr[j - 1] > arr[j]) {
                //swap elements  
                    temp = arr[j - 1];
                    arr[j - 1] = arr[j];
                    arr[j] = temp;
                    
                }

            } 
        } 
        return steps;
    }
    
    public static void inbuilt(int[] arr) {
        Arrays.sort(arr);
    }
    
    public static int selection(int[] arr) {
        int steps = 0;
        for(int x = 0; x < arr.length-1;x++){
           
            for(int i=x+1; i < arr.length;i++){
                steps++;
                if(arr[x]>arr[i]){
                    int temps = arr[x];
                    arr[x] = arr[i];
                    arr[i] = temps;
                }
            }
            
        }
        return steps;
    }
}
