/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arrays;

/**
 *
 * @author 23723
 */
public class Swap {
    public static void main(String[] args) {
        int[] array = {1,2,3,4};
        System.out.println(Arrays.toString(array));
        swap(array,1,2);
        System.out.println(Arrays.toString(array));
    }
    public static void swap(int[] a,int x, int y){
        int x2 = a[x];
        a[x] = a[y];
        a[y] = x2;
        
    }
}
