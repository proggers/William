/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author 23723
 */
public class Animal {
    private int numLegs, horns;
    private boolean mammal, carnivore;
    public double weight;
    
    public Animal(int l, boolean m, boolean c, int h, double w){
        numLegs = l;
        horns = h;
        mammal = m;
        carnivore = c;
        weight = w;
    }
    
    public void define(){
        if(numLegs==4){
            if(mammal){
                if(carnivore){
                    if(horns == 2){
                        if(weight>1000){
                            System.out.println("Not Defined");
                        } else {
                            System.out.println("Not Defined");
                        }
                    } else {
                        if(weight>1000){
                            System.out.println("Not Defined");
                        } else {
                            System.out.println("Wolverine");
                        }
                    }
                } else {
                    if(horns == 2){
                        if(weight>1000){
                            System.out.println("Giraffe");
                        } else {
                            System.out.println("Not Defined");
                        }
                    } else {
                        if(weight>1000){
                            System.out.println("Elephant");
                        } else {
                            System.out.println("Not Defined");
                        }
                    }
                }
            } else {
                if(carnivore){
                    if(horns == 2){
                        if(weight>1000){
                            System.out.println("Not Defined");
                        } else {
                            System.out.println("Komodo Dragon");
                        }
                    } else {
                        if(weight>1000){
                            System.out.println("Not Defined");
                        } else {
                            System.out.println("Not Defined");
                        }
                    }
                } else {
                    if(horns == 2){
                        if(weight>1000){
                            System.out.println("Not Defined");
                        } else {
                            System.out.println("Not Defined");
                        }
                    } else {
                        if(weight>1000){
                            System.out.println("Not Defined");
                        } else {
                            System.out.println("Not Defined");
                        }
                    }
                }
            }
        } else {
            if(mammal){
                if(carnivore){
                    if(horns == 2){
                        if(weight>1000){
                            System.out.println("Not Defined");
                        } else {
                            System.out.println("Not Defined");
                        }
                    } else {
                       if(weight>1000){
                            System.out.println("Not Defined");
                        } else {
                            System.out.println("Human");
                        } 
                    }
                } else {
                    if(horns == 2){
                        if(weight>1000){
                            System.out.println("Not Defined");
                        } else {
                            System.out.println("Not Defined");
                        }
                    } else {
                        if(weight>1000){
                            System.out.println("Not Defined");
                        } else {
                            System.out.println("Not Defined");
                        }
                    }
                }
            } else {
                if(carnivore){
                    if(horns == 2){
                        if(weight>1000){
                            System.out.println("Not Defined");
                        } else {
                            System.out.println("Not Defined");
                        }
                    } else {
                        if(weight>1000){
                            System.out.println("Not Defined");
                        } else {
                            System.out.println("Not Defined");
                        }
                    }
                } else {
                    if(horns == 2){
                        if(weight>1000){
                            System.out.println("Not Defined");
                        } else {
                            System.out.println("Not Defined");
                        }
                    } else {
                        if(weight>1000){
                            System.out.println("Not Defined");
                        } else {
                            System.out.println("Not Defined");
                        }
                    }
                }
            }
        }
    }
    
    public static void main(String[] args) {
        Animal animal = new Animal(4,true,false,2,1200.5);
        animal.define();
    }
}
