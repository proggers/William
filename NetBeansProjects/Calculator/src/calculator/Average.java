package calculator;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import javax.swing.JOptionPane;

/**
 *
 * @author 23723
 */
public class Average {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int num1 = Integer.parseInt(JOptionPane.showInputDialog("Enter First Number:"));//Inputs the numbers
        int num2 = Integer.parseInt(JOptionPane.showInputDialog("Enter Second Number:"));
        
            /**Integer.parseInt() converts string to integer
            *JOptionPane opens dialog box
            *showInputDialoge() gives 1 message and takes 1 input as a string value
            */
        
        int ave = (num1+num2)/2;//find average of the numbers
        
        System.out.println("The average is " + ave);//print the average
       
    }
    
}
