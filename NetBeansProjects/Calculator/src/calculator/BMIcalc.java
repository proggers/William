package calculator;

import javax.swing.JOptionPane;
/**
 *
 * @author William Moolman - UIV - 23723
 */
public class BMIcalc {
    public static void main(String[] args) {
        double weight = Double.parseDouble(JOptionPane.showInputDialog("Weight in KG's:"));
        double height = Double.parseDouble(JOptionPane.showInputDialog("Height in metres:"));
        double BMI = (double) weight/(height*height);
        
        if (BMI > 30) { System.out.println("A person weighing " + weight + "Kg and is " + height + "m tall");
            System.out.println("has a BMI of " + BMI + " is classified as: Obese");
        } else if (BMI >= 25) { System.out.println("A person weighing " + weight + "Kg and is " + height + "m tall");
            System.out.println("has a BMI of " + BMI + " is classified as: Overweight");
        } else if (BMI >= 18.5) {System.out.println("A person weighing " + weight + "Kg and is " + height + "m tall");
            System.out.println("has a BMI of " + BMI + " is classified as: Normal");
        } else if (BMI < 18.5) { System.out.println("A person weighing " + weight + "Kg and is " + height + "m tall");
            System.out.println("has a BMI of " + BMI + " is classified as: Underweight");
        } else { System.out.println("Error"); }
    } 
}
