package calculator;

public class Bicycle {
    
    int gear;
    int cadence;
    int speed;
    
    public Bicycle(int startCadence, int startSpeed, int startGear) {
        gear = startGear;
        cadence = startCadence;
        speed = startSpeed;
    }
    
    Bicycle myBike = new Bicycle(30,0,8);
}
