
package calculator;

public class Calculator {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        double pi = 3.1415926535;
        int radius = 42;
        double area = pi * radius * radius;
        
        System.out.println("A circle of radius " + radius + " has an area of " + area);

    }

}
