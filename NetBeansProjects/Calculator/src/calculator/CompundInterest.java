/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author 23723
 */
public class CompundInterest {
    public static void main(String[] args) {
        
        double P = 1500;
        double r = 0.043;
        int t = 6;
        int n = 4;
        
        double A = P*Math.pow((1+(r/n)),(n*t));
        
        System.out.println("Investing R" + P + " for " + t + " years at a annual"
                + " rate with quaterly investments at a rate of " + r*100 + "% ");
        System.out.println("will result in a final amount of R" + A);
        
        System.out.println();
        
        double P2 = 1000;
        double r2 = 0.043;
        int t2 = 0;
        int n2 = 4;
        
        double A2 = P2*Math.pow((1+(r2/n2)),(n2*t2));
       
        while (A2 < 1000000) {
            t2 = t2 + 1;
            A2 = P2*Math.pow((1+(r2/n2)),(n2*t2));
        }
        
        System.out.println("You would need to invest R" + P2 + " for " + t2 + 
                " years at a rate of " + r2*100 + "% per annum quaterly to reach R1 000 000");
    }
 
}
