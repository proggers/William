/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author 23723
 */
public class ConsecPrimes {
    public static boolean prime(int n) {
        
        int divisor = 1;
        int factors = 0;
        
        while (divisor <= n) {
            
            if (n % divisor == 0) {
                //System.out.println (divisor + " is a factor of " + n);
                factors = factors + 1;
            }
            divisor = divisor + 1;
        }
        
        if (factors == 2) {
            return true;
        } else {
            return false;
        }
       
    }    
    public static void main(String[] args) {
        int n = 2;
        int no = 0;
        int sum = 0;
        int largest = 0;
        while(sum<1000){
            if(prime(n)){
                sum += n;
                no++;
                if(prime(sum)&&sum<1000){
                    largest = sum;
                    
                }
            }
            n++;
        }
        System.out.println("Number: "+largest+", terms: "+no);
    }
}
