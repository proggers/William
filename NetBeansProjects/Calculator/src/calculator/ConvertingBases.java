/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import javax.swing.JOptionPane;

/**
 *
 * @author 23723
 */
public class ConvertingBases {
    public static void main(String[] args) {
        String[] options = new String[] {"Hexidecimal","Decimal","Binary"};
        
        int input = JOptionPane.showOptionDialog(null, "Input Type:", "Choose an option", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
        int output = JOptionPane.showOptionDialog(null, "Output Type:", "Choose an option", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
        String value = JOptionPane.showInputDialog("Input Value:"); 
        if (input == 0 && output == 0) { System.out.println(value);}
        if (input == 0 && output == 1) { hexDecimal(value);}
        if (input == 0 && output == 2) { }
        if (input == 1 && output == 0) { decimalHex(Integer.parseInt(value));}
        if (input == 1 && output == 1) { System.out.println(value);}
        if (input == 1 && output == 2) { }
        if (input == 2 && output == 0) { }
        if (input == 2 && output == 1) { }
        if (input == 2 && output == 2) { System.out.println(value);}
        
    }
    
    public static void hexDecimal(String hex) {
        int decimal = 0;
        int length = hex.length();
        hex.toUpperCase();
        for(int index=0; index<length; index++) {
            char digit = hex.charAt(index);
            
            int digitVal = digit - '0';
            if (digitVal > 9) {
                digitVal = 10 + (digit - 'A');
            }
            
            int power = (int)Math.pow(16, length - 1 - index);
            
            decimal = decimal + power * digitVal;
        }
        System.out.println("Hex = " + hex + ", decimal = " + decimal);
        
    }
    
    public static void decimalHex(int decimal) {
        int power = 1;
        int num = 1;
        while (true) {
            if ((int)Math.pow(16,power) > decimal) {
                break;
            } else {
                power++;
            }
        }
        int largestIndice = power-1;
        char[] hex;
        hex = new char[power];
        for (int indice = 0;indice <= largestIndice; indice++) {
            while(true) {
                power--;
                if (decimal-(int)Math.pow(16,power-1)*num < 0) {
                    System.out.println("Decimal - "+decimal);
                    System.out.println("num - "+num);
                    System.out.println("power - "+(int)Math.pow(16,power)*num);
                    power--;num--;
                    if (num < 10) { hex[indice] = (char)(num+48);}
                    if (num == 10) {hex[indice] = 'A';}
                    if (num == 11) {hex[indice] = 'B';}
                    if (num == 12) {hex[indice] = 'C';}
                    if (num == 13) {hex[indice] = 'D';}
                    if (num == 14) {hex[indice] = 'E';}
                    if (num == 15) {hex[indice] = 'F';}
                    else {hex[indice] = 'Z';}
                    decimal = decimal -(int)Math.pow(16,power+1)*num;
                    num=1;
                    break;
                } else {
                    num++;
                }
            }
            
        }
        System.out.println(hex[2]);System.out.println(hex[1]);System.out.println(hex[0]);    
    
    }
    
    
    public static int digits(int num) {
        int digits = 1;
        while(true) {
            if (num>10) { 
                num = num/10; digits++; 
            } else { 
                return digits;
            }
        }
    }
}
