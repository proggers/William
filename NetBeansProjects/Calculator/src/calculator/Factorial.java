/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import javax.swing.JOptionPane;

/**
 *
 * @author User
 */
public class Factorial {
    public static void main(String[] args) {
        int num = Integer.parseInt(JOptionPane.showInputDialog("Enter a number:"));
        int fact = num;
        num--;
        while (num > 0) {
            fact = fact * num;
            num--;
        }
        System.out.println(fact);
    }
}
