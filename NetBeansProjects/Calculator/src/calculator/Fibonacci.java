package calculator;

import javax.swing.JOptionPane;

public class Fibonacci {
    
    public static void main(String[] args) {
        
        int inputNum = Integer.parseInt(JOptionPane.showInputDialog("Length of Sequence:"));
        
        if (inputNum < 47 && inputNum >= 0) { //47 creates a number larger than int max
            fPrinter(inputNum);
        } else {
            while (inputNum >= 47 || inputNum < 0) {
                inputNum = Integer.parseInt(JOptionPane.showInputDialog("Please enter a number between 0 and 46:"));
            }
            fPrinter(inputNum);
        }
    }
    
    public static void fPrinter(int rep) {
        
        int num = 0;
        int first = 0;
        int second = 1;
        switch (rep) {
            case 1:
                System.out.println(first);
                break;
            case 2:
                System.out.println(first);
                System.out.println(second);
                break;
            default: 
                while (num <= rep) {
                    System.out.println(first);
                    num++;
                    if (num <= rep) {
                        System.out.println(second);
                    }
                    first = first + second;
                    second = first + second;
                    num++;
                    
                }       break;
        }
    }
}
