/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import javax.swing.JOptionPane;

/**
 *
 * @author 23723
 */
public class GuessingGame {
    public static void main(String[] args) {
        int again = 0;
        while (again == 0) {
            int number;
            int highscore = 10;
            number = (int)((int)20.0*Math.random());
            int guess;
            boolean gotIt = false;
            for (int turns = 5; turns > 0; turns--) {
                guess = Integer.parseInt(JOptionPane.showInputDialog("Pick a number. " + turns + " turns left"));
                int taken = 6 - turns;
                int left = turns - 1;
                
                if (guess == number) { 
                    JOptionPane.showMessageDialog(null, "Correct! " + taken + " turns taken");
                    gotIt = true;
                    if (taken<highscore) {
                        highscore = taken;
                        JOptionPane.showMessageDialog(null, "New highscore! " + taken + " turns");
                    } else {
                        JOptionPane.showMessageDialog(null, "Current highscore is " + taken); 
                    }
                    break;
                }
                else if (guess > number) {JOptionPane.showMessageDialog(null, "Too high! " + left + " turns left"); }
                else if (guess < number) {JOptionPane.showMessageDialog(null, "Too low! " + left + " turns left"); }
                else { System.out.println("Error"); }
            }
            if (!gotIt) {
                JOptionPane.showMessageDialog(null, "The answer was " + number);
            }
            again = JOptionPane.showConfirmDialog(null, "Play again?");
            gotIt = false;
            
        }
        JOptionPane.showMessageDialog(null, "Thanks for playing!");
    }
}

