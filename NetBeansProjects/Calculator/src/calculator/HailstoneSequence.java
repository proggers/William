/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

//import java.util.Scanner;
import javax.swing.JOptionPane;
/**
 *
 * @author 23723
 */
public class HailstoneSequence {
    
    public static void main(String[] args) {
        
        while (true) {
            
        int n = Integer.parseInt(JOptionPane.showInputDialog("Start Number:"));
        if (n < 0) {
            n = Integer.parseInt(JOptionPane.showInputDialog("Positive number, please:"));
        }
        //int n = new Scanner(System.in).nextInt();
        int counter = 0;
        
        System.out.println();
        System.out.println("Hailstone for " + n);
        System.out.println();
        
        while (n != 1) {
            
            counter = counter + 1;
            
            if (counter != 10) {
                System.out.print(n + ", ");
            } else {
                counter = 0;
                System.out.println(n + ", ");
            }
            
            if (n % 2 == 0) {
                n = n/2;
            } else {
                n = 3 * n + 1;
            }
            
          
            
        }
        
        System.out.println(n);
        
       
        }
    }
}
