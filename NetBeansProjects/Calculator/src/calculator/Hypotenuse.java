
package calculator;

import javax.swing.JOptionPane;

public class Hypotenuse {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        int side1 = Integer.parseInt(JOptionPane.showInputDialog("First side:"));
        int side2 = Integer.parseInt(JOptionPane.showInputDialog("Second side:"));
        int side3 = Integer.parseInt(JOptionPane.showInputDialog("Third side:"));
              
        if (side1>side2 && side1>side3 && side1*side1 == (side2*side2) + (side3*side3)) {
            System.out.println("Triangle is right angled and hypotenuse is " + side1);
        } else if (side2>side1 && side2>side3 && side2*side2 == (side2*side2) + (side1*side1)) {
            System.out.println("Triangle is right angled and hypotenuse is " + side2);
        } else if (side3>side2 && side3>side1 && side3*side3 == (side2*side2) + (side1*side1)) {
            System.out.println("Triangle is right angled and hypotenuse is " + side3);
        } else {
            System.out.println("Triangle is not right angled");
        }
        
    
    }       
    
    
}
