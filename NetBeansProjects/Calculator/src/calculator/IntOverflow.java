package calculator;

public class IntOverflow {
    public static void main(String[] args) {
        
        int Biggest = Integer.MAX_VALUE; //2147483647
        int Smallest = Integer.MIN_VALUE; //-2147483648
        
        long biggest = Long.MAX_VALUE; //9223372036854775807L;
        long smallest = Long.MAX_VALUE; //-9223372036854775808L;
        
        int N = 1;
        
        while(true) {
            int next = N + 1;
            if(next < 0) {
                System.out.println(N);
                System.out.println(next);
                System.exit(0);
            }
            
            N = next;
             
        }
    }
}
