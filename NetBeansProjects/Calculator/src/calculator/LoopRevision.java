/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author 23723
 */
public class LoopRevision {
    public static void main(String[] args) {
        int x = 23;
        while(true){
            if(test(x)){break;}
            x++;
        }
        System.out.println(x);
    }
    public static boolean test(int x){
        
        return sum(x)==product(x);
    }
    public static int sum(int x){
        int sum = 0;
        while (x>0){
            sum += x%10;
            x /= 10;
        }
        return sum;
    }
    public static int product(int x){
        int product = 1;
        while (x>0){
            product *= x%10;
            x /= 10;
        }
        return product;
    }
}
