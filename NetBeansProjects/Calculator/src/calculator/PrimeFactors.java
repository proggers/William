package calculator;

import javax.swing.JOptionPane;

public class PrimeFactors {
 
    public static void main(String[] args) {
        
        int n = Integer.parseInt(JOptionPane.showInputDialog("Enter a integer:"));
        int divisor = 2;
        int num = 0;
        
        while (divisor <= n) {
            if (n % divisor == 0) {
                num = num + 1;
                n = n/divisor;
            }
        }
        
    }
    
    public static void prime(int n) {
        
        int divisor = 1;
        int factors = 0;
        
        while (divisor <= n) {
            
            if (n % divisor == 0) {
                //System.out.println (divisor + " is a factor of " + n);
                factors = factors + 1;
            }
            divisor = divisor + 1;
        }
        
        if (factors == 2) {
            System.out.println(n + " is a prime number");
        } else {
            System.out.println(n + " is not a prime number");
        }
       
    }    
    
}
