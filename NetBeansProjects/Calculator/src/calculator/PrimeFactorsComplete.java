package calculator;
import javax.swing.JOptionPane;

public class PrimeFactorsComplete {
 
    public static void main(String[] args) {
        
        int n = Integer.parseInt(JOptionPane.showInputDialog("Enter a integer:"));
        int divisor = 2;
        int num = 0;
        int stopper = 0;
        
        while (divisor <= n) { //keeps going until no more possible divisors
            
            while (n % divisor == 0) {
                num = num + 1; //ounts number of factors of that prime
                n = n/divisor; //divides it as to make sure all factors are equal to num
            }
            if (num != 0) { //if there where factors of that prime run:
                if (stopper == 1) {
                    System.out.print("x");//adds the x, but not x3x4
                }
                stopper = 1;
                System.out.print("(" + divisor + "^" + num + ")");
                
            }
            divisor = divisor + 1; //adds one to number
            while (prime(divisor) == false) { //keeps going until number is prime
                divisor = divisor + 1;
            }
            
            num = 0; //resets factor count
        }
        
    }
        
    public static boolean prime(int n) {
        
        int divisor = 1;
        int factors = 0;
        
        while (divisor <= n){
            
            if (n % divisor == 0) {
                //System.out.println (divisor + " is a factor of " + n);
                factors = factors + 1;
            }
            divisor = divisor + 1;
        }
        
        return factors == 2;
       
    }    //Determines whether a num is prime
    
}
