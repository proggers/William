/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import javax.swing.JOptionPane;

/**
 *
 * @author 23723
 */
public class RunningAverage {
    public static void main(String[] args) {
        long num; int count = 1; double average; long sum = 0;
        while(true) {
            num = Long.parseLong(JOptionPane.showInputDialog("Enter a number:"));
            if (num == 0) {
                return;
            }
            sum = (long) sum + num;
            average = ((double) sum)/count;
            count++;
            JOptionPane.showMessageDialog(null, average);
        }
    }
}
