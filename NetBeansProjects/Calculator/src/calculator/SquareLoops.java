/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import javax.swing.JOptionPane;
/**
 *
 * @author 23723
 */
public class SquareLoops {
    
    public static void main(String[] args) {
        
        int n = 1;
        while (n*n < 100) {
            System.out.println("The square of " + n + " is " + n*n);
            //System.out.print(); will not move onto next line. 
            //Above statement could be written in several line
            n = n + 1;
        }
        
        JOptionPane.showMessageDialog(null, "First square not smaller than 100 is " + n*n);
    }
}
