package calculator;

import javax.swing.JOptionPane;

public class SwitchCaseRevision {
    public static void main(String[] args) {
        boolean cat = true;
        while(cat){
            int n = Integer.parseInt(JOptionPane.showInputDialog("Enter a month number"));
            String mnth;
            if(n<1||n>12){JOptionPane.showMessageDialog(null,"Please Enter a number between 1 and 12");}else{
                switch(n){
                    case 1:mnth="January";break;
                    case 2:mnth="February";break;
                    case 3:mnth="March";break;
                    case 4:mnth="April";break;
                    case 5:mnth="May";break;
                    case 6:mnth="June";break;
                    case 7:mnth="July";break;
                    case 8:mnth="August";break;
                    case 9:mnth="September";break;
                    case 10:mnth="October";break;
                    case 11:mnth="November";break;
                    case 12:mnth="December";break;
                    default: mnth = "";
                }
                switch(n){
                    case 12:case 1:case 2:System.out.println("Summer - "+mnth);break;
                    case 3:case 4:case 5:System.out.println("Autumn - "+mnth);break;
                    case 6:case 7:case 8:System.out.println("Winter - "+mnth);break;
                    case 9:case 10:case 11:System.out.println("Spring - "+mnth);break;
                }
                cat = false;
            }
        }
    }
}
