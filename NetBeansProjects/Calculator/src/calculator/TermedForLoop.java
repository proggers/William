/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author 23723
 */
public class TermedForLoop {
    public static void main(String[] args) {
        int terms = 15;
        double numerator = 1;
        double denominator = 2;
        double sum = 0;
        for(int term=1; term<=terms; term++){
            sum += (numerator/denominator);
            numerator++;
            denominator++;
        }
        System.out.println("1. " + sum);
        double one = sum;
        
        numerator = 2;
        denominator = 3;
        sum = 0;
        
        for(int term=1; term<=terms; term++){
            sum += numerator/denominator;
            numerator+=2;
            denominator+=2;
        }
        System.out.println("2. " + sum);
        double two = sum;
        
        numerator =3;
        denominator = 4;
        sum = 1/4;
        boolean neg = true;
        
        for(int term=1; term<=terms-1; term++){
            if(neg) {sum -= (numerator*numerator)/(denominator*denominator);}
            else{sum += (numerator*numerator)/(denominator*denominator);}
            numerator+=2;
            denominator+=2;
            
            neg = !neg;
        }
        System.out.println("3. " + sum);
        double three = sum;
        
        
        
    }
}
