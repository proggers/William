/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

/**
 *
 * @author 23723
 */
public class Types {
    
    public static void main(String[] args) {
        
        //Needed Types
        int a = 7;                      //Integers
        double x = 3.14159;             //Real Numbers
        String msg = "It's raining";    //Words and sentences
        char hash = '#';                //Single Character
        boolean question = true;        //True or False value
        
        //Unneeded Types and info
        byte b = 12;                    //Integer value that saves space, -128 to 127
        short s = 30000;                //Integer value that saves space, -2^15 to 2^15 -1
        int i = 2147483647;             //Main integer value, ranging from -2^31 to 2^31 -1
        long l = 900000000000000000L;   //Longer integer value, ranging from -2^63 to 2^63 -1   
        float f = 29.46f;               //A space saving decimal storing type
        
        //Mathematical functions
        System.out.println(Math.sin(3));            //sin and cos function
        System.out.println(Math.sqrt(7));           //Square root of int
        System.out.println(Math.abs(-23));          //absolute value of any num
        System.out.println(Math.random());          //random number forom 0 to 1
        System.out.println(Math.pow(2, 6));         //Raises on num(2) to another(6)
    }
    
}