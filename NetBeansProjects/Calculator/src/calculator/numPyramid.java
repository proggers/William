/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;


import javax.swing.JOptionPane;

/**
 *
 * @author 23723
 */
public class numPyramid {
    

    public static void main(String[] args) {
        
        
        String[] options = new String[] {"Basic","Hard"};
        int choice = JOptionPane.showOptionDialog(null, "What type?", "Choose an option", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
       
        int num = Integer.parseInt(JOptionPane.showInputDialog("How big?"));
        
        switch (choice){
            case 0: basic(num);break;
            case 1: hard(num);break;
        }
        
    }
    
    public static void hard(int num) {
        int count = 3;
        int overf = -2;
        
        for (int row = 1; row<=num;row++) {
            for (int column = row; column != 0; column--) {
                System.out.print("\t" + count);
                if(column != 1){count++;}
            }
            count -= overf;
            overf ++;
            System.out.println("");
        }
        
        
    }
    public static void basic(int num) {
        
        for(int row = 1; row<=num; row++) {
            for(int spaces = num - row; spaces!=0;spaces--){
                System.out.print(" ");
            }
            for (int count = 1; count<=row; count++) {
                System.out.print(count);
            }
            for (int count2 = row-1; count2>0;count2--) {
                System.out.print(count2);
            }
            System.out.println("");
        }
       
    }
}
