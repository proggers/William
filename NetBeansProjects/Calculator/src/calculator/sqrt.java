/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculator;

import javax.swing.JOptionPane;

/**
 *
 * @author 23723
 */
public class sqrt {
    public static void main(String[] args) {
        double x = 1;
        int y = Integer.parseInt(JOptionPane.showInputDialog("Number?"));
        double answer;
        int loops = 0;
        
        for(int S = 1; S<=y;S++) {
            answer = Math.sqrt(S);
            while(Math.abs(answer - x)>0.0000001) {
            x = 0.5*(x+S/x);
            loops++;
            }
            System.out.println("Number = " + S + "\t" + "Loops = " + loops + "\t answer = " + x);
        }
        
        
    }
}
