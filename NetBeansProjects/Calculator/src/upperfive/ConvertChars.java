/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package upperfive;

import javax.swing.JOptionPane;

/**
 *
 * @author 23723
 */
public class ConvertChars {
    public static void main(String[] args) {
        int k = JOptionPane.showInputDialog("Enter a character, or x to stop:").charAt(0);
        boolean go = true;
        if(k=='x'){go = false;}
        if(k=='X'){go = false;}
        while(go){
            if(Character.isDigit(k)){System.out.println(Character.toUpperCase(k));}
            if(Character.isLetter(k)){System.out.println(Math.sqrt(Character.getNumericValue(k)));}
            if(Character.isWhitespace(k)){System.out.println( "\\u" + Integer.toHexString(k | 0x10000).substring(1) );}
            k = JOptionPane.showInputDialog("Enter another character, or x to stop:").charAt(0);
            
            if(k=='x'){go = false;}
            if(k=='X'){go = false;}
        }
         
    }
}
