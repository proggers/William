/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package upperfive;

import javax.swing.JOptionPane;

/**
 *
 * @author 23723
 */
public class CountLetterDigits {
    public static void main(String[] args) {
        int digits = 0;
        int letters = 0;
        int others = 0;
        int k = Character.toUpperCase(JOptionPane.showInputDialog("Enter a character, or x to stop:").charAt(0));
        boolean go = true;
        if(k=='x'){go = false;}
        if(k=='X'){go = false;}
        while(go){
            if(Character.isDigit(k)){digits++;}
            if(Character.isLetter(k)){letters++;}
            else{others++;}
            k = Character.toUpperCase(JOptionPane.showInputDialog("Enter another character, or x to stop:").charAt(0));
            
            if(k=='x'){go = false;}
            if(k=='X'){go = false;}
        }
        System.out.println("Digits: "+digits);
        System.out.println("Letters: "+letters);
        System.out.println("Other Characters: "+others);
        
    }
}
