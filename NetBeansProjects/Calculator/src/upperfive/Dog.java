/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package upperfive;

import javax.swing.JOptionPane;

/**
 *
 * @author 23723
 */
public class Dog {
    private String furColour;
    private String eyeColour;
    private String breed;
    private String name;
    private int energy;
    private int fitness;
    private int hunger;
    private boolean trained;
    private boolean mute;
    
    
    public Dog(String f,String e,String b,String n,boolean t){
        furColour = f;
        eyeColour = e;
        breed = b;
        name = n;
        energy = 100;
        fitness = 50;
        hunger = 100;
        trained = t;
        mute = false;
    }
    
    public void live(){
        fitness-=10;
        hunger-=10;
        if (hunger==0){System.out.println(name+" is starving");}
        if (hunger<0){System.out.println(name+" starved to death.");System.out.println("...................THE END...................");System.exit(0);}
    }
    
    public void bark(){
        live();
        if(!mute){
            System.out.println(name+": 'Bark! Bark!'");
        } else {
            System.out.println("You told "+name+" to stfu, so he's not going to bark.");
        }
    }
    public void feed(){
        System.out.println("You fed "+name);
        hunger=110;
        live();
    }
    public void walk(){
        live();
        if(energy>=10){
            if(fitness!=100){
                energy -= 10;
                fitness += 20;
                System.out.println(name+" walked.");
            } else {
                System.out.println(name+" is fully exercised.");
            }
        } else {
            System.out.println(name+" is too tired.");
        }
            
    }
    public void sit(){
        live();
        if(trained){
            System.out.println(name+" sits.");
        } else {
            System.out.println(name+" is not trained.");
        }
    }
    public void train(){
        live();
        if(trained){
            System.out.println(name+" is already trained.");
        } else {
            System.out.println("You succesfully trained "+name);
            trained = true;
        }
    }
    public void stfu(){
        live();
        System.out.println(name+" won't speak anymore.");
        mute = true;
    }
    public void nap(){
        energy=110;
        live();
        System.out.println(name+" had a nice nap.");
    }
    
    
    public void playtime(){
        System.out.println("");
        String[] options = new String[]{"Bark","Walk","Sit","Train","Stfu","Nap","Back"};
        while(true){
            int x = JOptionPane.showOptionDialog(null, null,name+" the dog",
                        JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
            switch(x){
                case 0:bark();break;
                case 1:walk();break;
                case 2:sit();break;
                case 3:train();break;
                case 4:stfu();break;
                case 5:nap();break;
                case 6:System.out.println("You stop playing with "+name+".");return;
            }
        }
    }
    
    public void mood(){
        System.out.println("");
        String m = "";
        if(mute){m="Yes";}else{m="No";}
        System.out.println("Name: "+name);
        System.out.println("Energy: "+energy+"%");
        System.out.println("Fitness: "+fitness+"%");
        System.out.println("Food: "+hunger+"%");
        System.out.println("Mute: "+m);
    }
    
    public void traits(){
        System.out.println("");
        System.out.println("Name: "+name);
        System.out.println("Fur Colour: "+furColour);
        System.out.println("Eye Colour: "+eyeColour);
        System.out.println("Breed: "+breed);
    }
    
    public void mainMenu(){
        System.out.println("This is the beginning of your story with "+name+".");
        System.out.println("");
        String[] options = new String[]{"Traits","Playtime","Mood","Kill "+name};
        while(true){
            int x = JOptionPane.showOptionDialog(null, null,name+" the dog",
                        JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, options, options[0]);
            switch(x){
                case 0:traits();break;
                case 1:playtime();break;
                case 2:mood();break;
                case 3:System.out.println("You killed "+name+".");System.out.println("...................THE END...................");return;
            }
        }
    }
    
    public static void main(String[] args) {
        Dog steve = new Dog("Brown","Red","Border Collie","Steve",false);
        steve.mainMenu();
    }
}
