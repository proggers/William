/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package upperfive;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author 23723
 */
public class HighScores {
    String[] name;
    int[] score;
    String[] time;
    int l;
    public HighScores(File f) throws FileNotFoundException{
        l = length(f);
        name = new String[l];
        score = new int[l];
        time = new String[l];
        Scanner sc = new Scanner(f);
        for(int i  = 0;i<l;i++){
            String k = sc.nextLine();
            Scanner sl = new Scanner(k).useDelimiter("#");
            name[i]=sl.next();
            score[i]=sl.nextInt();
            time[i]=sl.next();
            sl.close();
        }
    }
    public static int length(File f) throws FileNotFoundException{
        int count = 0;
        Scanner sc = new Scanner(f);
        while(sc.hasNextLine()){
            count++;
            sc.nextLine();
        }
        return count;
    }
    public static void pt(){
        System.out.println("Name: \t\t Score: \t Time");
        System.out.println("---------------------------------------");
        
    }
    public void rank(int i){
        pt();
        //System.out.println(name[i-1]+" \t\t "+score[i-1]+" \t\t "+time[i-1]);
        System.out.printf("%-17s%-16s%-16s\n", name[i-1], score[i-1],time[i-1]);
    }
    public void lb(){
        pt();
        for(int i = 0;i<l;i++){
            //System.out.println(name[i]+" \t\t "+score[i]+" \t\t "+time[i]);
            System.out.printf("%-17s%-16s%-16s\n", name[i], score[i],time[i]);
        }
    }
    public static void main(String[] args) throws FileNotFoundException {
        File f = new File("HiScores.txt");
        HighScores hs = new HighScores(f);
        //hs.rank(1);
        hs.lb();
    }
}
