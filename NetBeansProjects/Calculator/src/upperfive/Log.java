/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package upperfive;

import java.util.Scanner;
import java.io.*;

/**
 *
 * @author 23723
 */
public class Log {

    String[] names;
    String[] score;
    String[] time;
    File file;

    public Log(String f) throws FileNotFoundException {
        file = new File(f);
        Scanner Scfile = new Scanner(file);
        Scfile.useDelimiter("#");
        int line = lines(f);
        names = new String[line];
        score = new String[line];
        time = new String[line];
        for (int x = 0; x < line; x++) {
            names[x] = Scfile.next();
            score[x] = Scfile.next();
            time[x] = Scfile.next();
        }
    }

    public static int lines(String f) throws FileNotFoundException {
        Scanner Scfile = new Scanner(new File(f));
        Scfile.useDelimiter("#");
        int lines = 0;
        while (Scfile.hasNextLine()) {
            lines++;
            Scfile.nextLine();
        }
        return lines;
    }

    public void read(int a) {
        System.out.println("Person:\t" + names[a]);
        System.out.println("Code:\t" + score[a]);
        System.out.println("Time:\t" + time[a]);
    }

    public static void main(String[] args) throws FileNotFoundException {
        Log hs = new Log("HiScores.txt");
        hs.read(0);
    }

}
