/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package upperfive;

/**
 *
 * @author 23723
 */
public class TestCharacters {
    
    public static void main(String[] args) {
        char ch = '3';
        if(Character.isLetter(ch)){
            System.out.println(ch+" in uppercase is "+Character.toUpperCase(ch));
        } else {
            System.out.println("No uppercase for a non-letter character such as "+ch);
        }
    }
    
}
