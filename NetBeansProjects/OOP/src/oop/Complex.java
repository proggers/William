/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop;

/**
 *
 * @author 23723
 */
public class Complex {
    
    //Properties
    private double a;
    private double b;
    
    //Constructor
    public Complex(double real, double imaginary) {
        a = real;
        b = imaginary;
        System.out.println("Made a new complex: " + a + " + " + b + "i.");
    }
    
    public double size() {
        return Math.sqrt(a*a + b*b);
    }
    
    //Main
    public static void main(String[] args) {
        double x = 3.14;
        Complex z = new Complex(-2,3);
        Complex z2 = new Complex(1,2);
        System.out.println("Size of z: " + z.size());
        System.out.println("Size of z: " + z.size());
    }
}
