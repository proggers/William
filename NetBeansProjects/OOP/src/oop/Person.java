/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop;

/**
 *
 * @author 23723
 */
public class Person {
    
    double height;
    double weight;
    String name;
    boolean isDepressed;
    
    
    public Person(double h, double w, String s) {
        height = h;
        weight = w;
        name = s;
    }
    
    public void becomesDepressed(){
        isDepressed = true;
    }
    
    public void medication(){
        isDepressed = false;
    }
    @Override
    public String toString() {
        return "Name: " + name + "; Weight: " + weight + "kg; Height: " + height + "m;";
    }
    
    public double bmi() {
        return weight / (height*height);
    }
    
    public static void makePeople(){
        Person Simon = new Person(1.9,69,"Simon");
        Person James = new Person(1.2,40,"James");
        Person Dylan = new Person(0.4,10,"Dylan");
        
    }
    public static void main(String[] args) {
        makePeople();
        
        Person Gordon = new Person(1.9,80,"Gordon");
        Gordon.becomesDepressed();
    }
}
