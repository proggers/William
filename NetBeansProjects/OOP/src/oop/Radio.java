/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author 23723
 */
public class Radio {
    int volume;
    double frequency;
    double[] preset = new double[6];
    boolean on;
    
    public Radio (){
        volume = 0;
        frequency = 0;
        on = false;
    }    
    public void setVolume(int v){
        volume = v;
    }    
    public void changeVolumeBy(int v){
        volume += v;
    }    
    public void tuneTo(double f){
        frequency = f;
    }    
    public void tuneToPreset(int p){
        frequency = preset[p-1];
    }    
    public void savePreset(int p){
        preset[p-1] = frequency;
    }  
    public boolean isOn(){
        return on;
    }
    public void togglePower(){
        on = !on;
    }
    public String toString() {
        String power = "error";
        String currentPreset;
        if (on == false){power = "Off";}
        if (on == true){power = "On";}
        if(frequency==preset[0]){currentPreset="1";}
        else if(frequency==preset[1]){currentPreset="2";}
        else if(frequency==preset[2]){currentPreset="3";}
        else if(frequency==preset[3]){currentPreset="4";}
        else if(frequency==preset[4]){currentPreset="5";}
        else if(frequency==preset[5]){currentPreset="6";}
        else {currentPreset = "none";}
        return "Radio power: ["+power+"] volume: ["+volume+"] frequency: ["+frequency+"] preset: ["+currentPreset+"]";
    }
    
    public static void main(String[] args) {
        Radio r = new Radio();
        r.setVolume(5);
        r.tuneTo(94.7);
        r.savePreset(1);
        r.tuneTo(107.5);
        r.tuneToPreset(1);
        r.togglePower();
        System.out.println(r);
        
    }
}
