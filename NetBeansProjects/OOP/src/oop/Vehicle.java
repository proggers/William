/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package oop;

/**
 *
 * @author 23723
 */
public class Vehicle {
    int fuel;
    double economy;
    
    public Vehicle(int f,double e){
        fuel = f;
        economy = e;
    }
    public void fillTank(int f){
        fuel += f;
    }
    
    public double getRange(){
        return economy*fuel;
    }
    public static void main(String[] args) {
        Vehicle[] fleet = new Vehicle[3];
        fleet[0] = new Vehicle(50,14.4);
        fleet[1] = new Vehicle(80,9.7);
        fleet[2] = new Vehicle(60,12.3);
        double maxrange = 0;
        for(int x = 0; x<3;x++){
            if(fleet[x].getRange()>maxrange){maxrange=fleet[x].getRange();}
            
        }
        System.out.println(maxrange);
    }
}
