/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rugbytries;

import javax.swing.JOptionPane;

/**
 *
 * @author 23723
 */
public class RugbyTriesForLoops {
    public static void main(String[] args) {
        int combinations = 0;
        int score = Integer.parseInt(JOptionPane.showInputDialog("Enter a score"));
        if (score == 0) { System.out.println("Nothing scored"); } else {
            if (score >= 0) {
                for (int penalties = 0; penalties <= score/3; penalties ++) {
                    for ( int tries = 0; tries <= score/5; tries++) {
                        for ( int convertedTries = 0; convertedTries <= score/7; convertedTries ++) {
                            if (penalties*3+tries*5+convertedTries*7 == score ) {
                                System.out.println(penalties +" penalties, " + tries + " tries, and " + convertedTries + " converted tries");
                                combinations ++;
                            }
                        }
                    }
                }
                if ( combinations == 0) {System.out.println("Not a valid score");}
            } else { System.out.println("Not a valid score"); }
        }
    }
}
