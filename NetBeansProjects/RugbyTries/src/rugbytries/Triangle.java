/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rugbytries;

import javax.swing.JOptionPane;

/**
 *
 * @author 23723
 */
public class Triangle {
    public static void main(String[] args) {
        String[] options = new String[] {"Right angle triangle","Pretty triangle","Hollow Square"};
        int choice = JOptionPane.showOptionDialog(null, "What type?", "Choose an option", JOptionPane.DEFAULT_OPTION, JOptionPane.PLAIN_MESSAGE, null, options, options[0]);
        int num = Integer.parseInt(JOptionPane.showInputDialog("Enter a number:"));
        switch (choice){
            case 0: rightAngled(num); break;
            case 1: prettyTriangle(num); break;
            case 2: hollowSquare(num); break;
            
        }
    }
    
    public static void rightAngled(int num) {
        
        for(int x = 1; x <= num; x++) {
            for (int y = 1; y <= x; y++) {
                System.out.print("* ");
            }
            System.out.println("");
        }
    }
    
    public static void prettyTriangle(int num) {
        int spaces = -1;
        for (int x = num; x > 0; x--) {
            for (int spacesMade = 0; spacesMade <= spaces; spacesMade++){
                    System.out.print(" ");
                }
            for (int y = x; y > 0; y--) {
                
                System.out.print("*");
                System.out.print(" ");
            }
            System.out.println("");
            spaces++;
        }
    }
    
    public static void hollowSquare(int num) {
        
        for (int lineNo = 1; lineNo <= num; lineNo++) {
            if (lineNo == 1) {
                for (int x = 1; x <= num; x++) {
                    System.out.print("* ");
                }
                System.out.println("");
            } else if (lineNo == num) {
                for (int x = 1; x <= num; x++) {
                    System.out.print("* ");
                }
                System.out.println("");
            } else {
                System.out.print("* ");
                for (int x = 1; x <= num-2; x++) {
                    System.out.print("  ");
                }
                System.out.println("*");
            }
            
        }
    }
}   
    

