/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stringtest;

import javax.swing.JOptionPane;

/**
 *
 * @author 23723
 */
public class Q1 {
    public static void main(String[] args) {
        String k = JOptionPane.showInputDialog("Enter a sentence:");
        int nonAlpha = 0;
        int caps = 0;
        int fourLetter = 0;
        int letterCount = 0;
        for(int i = 0;i<k.length();i++){
            char j = k.charAt(i);
            if(Character.isUpperCase(j)){
                caps++;
            }
            if(!Character.isLetter(j)){
                nonAlpha++;
            }
            if(Character.isLetter(j)){
                letterCount++;
            } else {
                if(letterCount == 4){
                    fourLetter++;
                }
                letterCount=0;
            }
        }
        if(letterCount == 4){
            fourLetter++;
        }
        System.out.println("Non Alphabetical characters: "+nonAlpha);
        System.out.println("Capital letters: "+caps);
        System.out.println("Four letter words: "+fourLetter);
    }
}
